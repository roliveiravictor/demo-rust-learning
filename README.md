# README #
Rust - Samples.

Programming Language: Rust

Editor:

IntelliJ IDEA 2018.2.3 (Community Edition)
Build #IC-182.4323.46, built on September 3, 2018
JRE: 1.8.0_152-release-1248-b8 x86_64
JVM: OpenJDK 64-Bit Server VM by JetBrains s.r.o
macOS 10.14.1

Rust Version: 1.30.1

### What is this repository for? ###

   * This is a backup of my rust samples
   * Final version

### How do I get set up? ###

   * https://doc.rust-lang.org/book/

### Contribution guidelines ###

   * N/A

### Who do I talk to? ###

   * roliveira.victor@gmail.com

### Youtube ###

   *  N/A

### Prints ###

   *  N/A
